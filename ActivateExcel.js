﻿let myExcel, worksheet, workbook;

function ReadDataFromExcel(filePath,sheetName)
{
   myExcel = getActiveXObject("Excel.Application");
   myExcel.Visible = true;
   workbook = myExcel.Workbooks.Open(filePath);
   worksheet = workbook.Sheets.Item(sheetName);
   workbook.Sheets.Item(sheetName).Activate;
   return {worksheet,workbook, myExcel};
  
}  
 
  
module.exports.ReadDataFromExcel = ReadDataFromExcel;